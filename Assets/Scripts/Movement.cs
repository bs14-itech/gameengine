using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour
{

    public float velocity = 10f;
    Vector3 direction = new Vector3(1f, 0f, 0f);
	
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        this.transform.position += direction * velocity * Time.deltaTime;
	}
}
