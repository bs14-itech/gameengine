using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{

    public float velocity = 2f;
    Vector3 direction = new Vector3(1f, 0f, 0f);
    Vector3 endposition;
    Vector3 startposition;
    bool ghost = false;
    public Sprite newSprite;
    SpriteRenderer m_SpriteRenderer;
    Animator m_Animator;
	
    // Start is called before the first frame update
    void Start()
    {
    	startposition = this.transform.position;
    	endposition = startposition + new Vector3(4f, 0f, 0f);
    	m_SpriteRenderer = GetComponent<SpriteRenderer>();
    	m_Animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
    	if (ghost == false){
		    this.transform.position += direction * velocity * Time.deltaTime;
		    if (this.transform.position.x > endposition.x){
		    	direction = new Vector3(-1f, 0f, 0f);
		    	m_SpriteRenderer.flipX = false;
		    	
		    }
		    if (this.transform.position.x < startposition.x){
		    	direction = new Vector3(1f, 0f, 0f);
		    	m_SpriteRenderer.flipX = true;
		    }
        }
    }
    
    void OnTriggerEnter2D (Collider2D other){
    // Checking if the overlapped collider is an enemy
    if (other.CompareTag ("Player")) {
      ghost = true;
      m_Animator.enabled = false;
      m_SpriteRenderer.sprite = newSprite;
    }
  }
}
