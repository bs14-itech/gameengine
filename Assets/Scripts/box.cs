using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; // This is very important if we want to restart

public class box : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (this.transform.position.y < -22f){
	  		SceneManager.LoadScene ("fkaptein");
        }
        if (this.transform.position.x < -10f){
        	this.transform.position = new Vector3(-10f, this.transform.position.y, 0f);
        }
        if (this.transform.position.x > 69f){
        	this.transform.position = new Vector3(69f, this.transform.position.y, 0f);
        }
    }
    
    void OnTriggerEnter2D (Collider2D other){
    // Checking if the overlapped collider is an enemy
	if (other.CompareTag ("Enemy")) {
	  // This scene HAS TO BE IN THE BUILD SETTINGS!!!
	  SceneManager.LoadScene ("fkaptein");
	}
  }
}
